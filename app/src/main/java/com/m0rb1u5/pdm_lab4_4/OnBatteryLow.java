package com.m0rb1u5.pdm_lab4_4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class OnBatteryLow extends BroadcastReceiver {
   public OnBatteryLow() {
   }

   @Override
   public void onReceive(Context context, Intent intent) {
      // TODO: This method is called when the BroadcastReceiver is receiving
      // an Intent broadcast.
      Log.d("App", "Recibido!");
      Toast.makeText(context, "La batería esta baja, conectar al cargador!", Toast.LENGTH_LONG).show();
   }
}
